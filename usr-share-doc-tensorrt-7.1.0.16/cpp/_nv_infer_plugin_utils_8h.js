var _nv_infer_plugin_utils_8h =
[
    [ "Quadruple", "structnvinfer1_1_1plugin_1_1_quadruple.html", "structnvinfer1_1_1plugin_1_1_quadruple" ],
    [ "PriorBoxParameters", "structnvinfer1_1_1plugin_1_1_prior_box_parameters.html", "structnvinfer1_1_1plugin_1_1_prior_box_parameters" ],
    [ "RPROIParams", "structnvinfer1_1_1plugin_1_1_r_p_r_o_i_params.html", "structnvinfer1_1_1plugin_1_1_r_p_r_o_i_params" ],
    [ "GridAnchorParameters", "structnvinfer1_1_1plugin_1_1_grid_anchor_parameters.html", "structnvinfer1_1_1plugin_1_1_grid_anchor_parameters" ],
    [ "DetectionOutputParameters", "structnvinfer1_1_1plugin_1_1_detection_output_parameters.html", "structnvinfer1_1_1plugin_1_1_detection_output_parameters" ],
    [ "softmaxTree", "structnvinfer1_1_1plugin_1_1softmax_tree.html", "structnvinfer1_1_1plugin_1_1softmax_tree" ],
    [ "RegionParameters", "structnvinfer1_1_1plugin_1_1_region_parameters.html", "structnvinfer1_1_1plugin_1_1_region_parameters" ],
    [ "NMSParameters", "structnvinfer1_1_1plugin_1_1_n_m_s_parameters.html", "structnvinfer1_1_1plugin_1_1_n_m_s_parameters" ],
    [ "CodeTypeSSD", "_nv_infer_plugin_utils_8h.html#a7945a8dcf85016be45251204c54c6b9f", [
      [ "CORNER", "_nv_infer_plugin_utils_8h.html#a7945a8dcf85016be45251204c54c6b9fac411afd31d32cec664d372acc12f404a", null ],
      [ "CENTER_SIZE", "_nv_infer_plugin_utils_8h.html#a7945a8dcf85016be45251204c54c6b9fa1150a8d7752b01d30d91fe18fe9d8a54", null ],
      [ "CORNER_SIZE", "_nv_infer_plugin_utils_8h.html#a7945a8dcf85016be45251204c54c6b9fafbc6c35854fe02eb9e792f897399c42a", null ],
      [ "TF_CENTER", "_nv_infer_plugin_utils_8h.html#a7945a8dcf85016be45251204c54c6b9fae65a115db9b4bcc4ae0aa0b989089d16", null ]
    ] ],
    [ "PluginType", "_nv_infer_plugin_utils_8h.html#afeb8e3947449a5adfec9a4f06324bad0", [
      [ "kFASTERRCNN", "_nv_infer_plugin_utils_8h.html#afeb8e3947449a5adfec9a4f06324bad0af5aaff476d9933081c34d20339db7a20", null ],
      [ "kNORMALIZE", "_nv_infer_plugin_utils_8h.html#afeb8e3947449a5adfec9a4f06324bad0a134db1ed26a3f975113787ed287669f5", null ],
      [ "kPERMUTE", "_nv_infer_plugin_utils_8h.html#afeb8e3947449a5adfec9a4f06324bad0a19e052e3047ef66b0c0b67d39517ae39", null ],
      [ "kPRIORBOX", "_nv_infer_plugin_utils_8h.html#afeb8e3947449a5adfec9a4f06324bad0a985b991de0557b74c0122f705813be25", null ],
      [ "kSSDDETECTIONOUTPUT", "_nv_infer_plugin_utils_8h.html#afeb8e3947449a5adfec9a4f06324bad0acac523cf5c0dacec87c8d5f03099b278", null ],
      [ "kCONCAT", "_nv_infer_plugin_utils_8h.html#afeb8e3947449a5adfec9a4f06324bad0a429fb1a42c86e15f4cdbde3f339cdd1a", null ],
      [ "kPRELU", "_nv_infer_plugin_utils_8h.html#afeb8e3947449a5adfec9a4f06324bad0a672a33ce7d94652bf08c12b2bab93b7a", null ],
      [ "kYOLOREORG", "_nv_infer_plugin_utils_8h.html#afeb8e3947449a5adfec9a4f06324bad0a2ea4f73960aacbbc3e7f1b65447bb865", null ],
      [ "kYOLOREGION", "_nv_infer_plugin_utils_8h.html#afeb8e3947449a5adfec9a4f06324bad0aa62a73352ec06ba83d76dd1cf9896f95", null ],
      [ "kANCHORGENERATOR", "_nv_infer_plugin_utils_8h.html#afeb8e3947449a5adfec9a4f06324bad0a738cc63924d0d98dc1871c11d25bdc11", null ]
    ] ],
    [ "EnumMax< PluginType >", "_nv_infer_plugin_utils_8h.html#a6eb8d2dd106bfa792e3bad97ffa8b0a7", null ]
];