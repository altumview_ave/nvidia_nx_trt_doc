var _nv_onnx_parser_8h =
[
    [ "IParserError", "classnvonnxparser_1_1_i_parser_error.html", "classnvonnxparser_1_1_i_parser_error" ],
    [ "IParser", "classnvonnxparser_1_1_i_parser.html", "classnvonnxparser_1_1_i_parser" ],
    [ "NV_ONNX_PARSER_MAJOR", "_nv_onnx_parser_8h.html#ae3846741c54feda6c7b579d312b1c934", null ],
    [ "NV_ONNX_PARSER_MINOR", "_nv_onnx_parser_8h.html#ab8c94850c9cafc5d84d0cca204085a45", null ],
    [ "NV_ONNX_PARSER_PATCH", "_nv_onnx_parser_8h.html#a99412a6b02d6354f339da73db1995dea", null ],
    [ "SubGraph_t", "_nv_onnx_parser_8h.html#adb268c1263c9fa13d7e6f7127c4ae462", null ],
    [ "SubGraphCollection_t", "_nv_onnx_parser_8h.html#a65ce8c57e8d4dbff5fbe2aff492202b9", null ],
    [ "ErrorCode", "_nv_onnx_parser_8h.html#a4436c958da87fa00c3eff469da2eea4d", [
      [ "kSUCCESS", "_nv_onnx_parser_8h.html#a4436c958da87fa00c3eff469da2eea4dac4b67917e88068716be9d96ab74529ab", null ],
      [ "kINTERNAL_ERROR", "_nv_onnx_parser_8h.html#a4436c958da87fa00c3eff469da2eea4dab68250d401887ca34ce3d50f23dea324", null ],
      [ "kMEM_ALLOC_FAILED", "_nv_onnx_parser_8h.html#a4436c958da87fa00c3eff469da2eea4da3762bcc521c367dcc1e4cc8e8a89712f", null ],
      [ "kMODEL_DESERIALIZE_FAILED", "_nv_onnx_parser_8h.html#a4436c958da87fa00c3eff469da2eea4da960c2eeac9520bfeab4e8a3c3402628a", null ],
      [ "kINVALID_VALUE", "_nv_onnx_parser_8h.html#a4436c958da87fa00c3eff469da2eea4dab6d8598f8b6b2da7ffed0d72cd62d118", null ],
      [ "kINVALID_GRAPH", "_nv_onnx_parser_8h.html#a4436c958da87fa00c3eff469da2eea4da1a7e396cf298e3a1f724fe1713a32474", null ],
      [ "kINVALID_NODE", "_nv_onnx_parser_8h.html#a4436c958da87fa00c3eff469da2eea4da600bbb31cbb1e4d4d286733b4f6a5dc5", null ],
      [ "kUNSUPPORTED_GRAPH", "_nv_onnx_parser_8h.html#a4436c958da87fa00c3eff469da2eea4da02262532bc3c299fc03132f8990a19d2", null ],
      [ "kUNSUPPORTED_NODE", "_nv_onnx_parser_8h.html#a4436c958da87fa00c3eff469da2eea4da9e5764a6e9b5a2e62f1e883c7aa26bca", null ]
    ] ],
    [ "createNvOnnxParser_INTERNAL", "_nv_onnx_parser_8h.html#a4057ac6bb59bad693776e4a70b346fdd", null ],
    [ "createParser", "_nv_onnx_parser_8h.html#a25ed46b279f6a1bd1e9185764e4145ad", null ],
    [ "EnumMax", "_nv_onnx_parser_8h.html#a58cb25b1ef4acd84e91db81e99c89b39", null ],
    [ "EnumMax< ErrorCode >", "_nv_onnx_parser_8h.html#a42e41fea3fb558a50e00bd334e2089cd", null ],
    [ "getNvOnnxParserVersion", "_nv_onnx_parser_8h.html#ad47c3aab5254ec350fc0c0861b49c095", null ]
];