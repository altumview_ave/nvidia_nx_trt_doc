var _nv_uff_parser_8h =
[
    [ "FieldMap", "classnvuffparser_1_1_field_map.html", "classnvuffparser_1_1_field_map" ],
    [ "FieldCollection", "structnvuffparser_1_1_field_collection.html", "structnvuffparser_1_1_field_collection" ],
    [ "IPluginFactory", "classnvuffparser_1_1_i_plugin_factory.html", "classnvuffparser_1_1_i_plugin_factory" ],
    [ "IPluginFactoryExt", "classnvuffparser_1_1_i_plugin_factory_ext.html", "classnvuffparser_1_1_i_plugin_factory_ext" ],
    [ "IUffParser", "classnvuffparser_1_1_i_uff_parser.html", "classnvuffparser_1_1_i_uff_parser" ],
    [ "UFF_REQUIRED_VERSION_MAJOR", "_nv_uff_parser_8h.html#a8f73f2e7ffd384940f88c11ed12e4651", null ],
    [ "UFF_REQUIRED_VERSION_MINOR", "_nv_uff_parser_8h.html#a0e638178e19d253f0096701f345c2d6f", null ],
    [ "UFF_REQUIRED_VERSION_PATCH", "_nv_uff_parser_8h.html#a7802cced2d26485d4375656491b08113", null ],
    [ "FieldType", "_nv_uff_parser_8h.html#a452d6bf18c1dbd7d6936c424b9ae6faf", [
      [ "kFLOAT", "_nv_uff_parser_8h.html#a452d6bf18c1dbd7d6936c424b9ae6fafa2963c06d855a6b49f4b1abe2010e90b5", null ],
      [ "kINT32", "_nv_uff_parser_8h.html#a452d6bf18c1dbd7d6936c424b9ae6fafabd073fcbb15020b25a70e2cd95f9f4a9", null ],
      [ "kCHAR", "_nv_uff_parser_8h.html#a452d6bf18c1dbd7d6936c424b9ae6fafa34bfd5695ffc553b154d578fd777eaa5", null ],
      [ "kDIMS", "_nv_uff_parser_8h.html#a452d6bf18c1dbd7d6936c424b9ae6fafa51a7ce7695ea4679651fc509ffe1687b", null ],
      [ "kDATATYPE", "_nv_uff_parser_8h.html#a452d6bf18c1dbd7d6936c424b9ae6fafae6673e5d36ec73f3f9b1c53b50b381cb", null ],
      [ "kUNKNOWN", "_nv_uff_parser_8h.html#a452d6bf18c1dbd7d6936c424b9ae6fafa16bb40b9102367393ecf8213078d1c6e", null ]
    ] ],
    [ "UffInputOrder", "_nv_uff_parser_8h.html#ae6a2a9503d69a32179de572c67b0b8be", [
      [ "kNCHW", "_nv_uff_parser_8h.html#ae6a2a9503d69a32179de572c67b0b8beacaf70f83fa10041f93bb2ee89848d4b9", null ],
      [ "kNHWC", "_nv_uff_parser_8h.html#ae6a2a9503d69a32179de572c67b0b8beaf6d739344f54ea3cddfa1f02f7f39b0a", null ],
      [ "kNC", "_nv_uff_parser_8h.html#ae6a2a9503d69a32179de572c67b0b8bea60937acb83d14142c2bcbbfd9ef49f46", null ]
    ] ],
    [ "createNvUffParser_INTERNAL", "_nv_uff_parser_8h.html#a053d2d374ac0faff6d5d62b6c4596adc", null ],
    [ "createUffParser", "_nv_uff_parser_8h.html#ad8cf8b2c4ec297b910068956c60367d6", null ],
    [ "shutdownProtobufLibrary", "_nv_uff_parser_8h.html#ab2fcaa6b591c22c559615a7e4101495f", null ]
];