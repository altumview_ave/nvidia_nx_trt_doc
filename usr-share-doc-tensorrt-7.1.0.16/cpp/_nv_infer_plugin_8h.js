var _nv_infer_plugin_8h =
[
    [ "INvPlugin", "classnvinfer1_1_1plugin_1_1_i_nv_plugin.html", "classnvinfer1_1_1plugin_1_1_i_nv_plugin" ],
    [ "createAnchorGeneratorPlugin", "_nv_infer_plugin_8h.html#a12c86c156ae0ab81e65d59f3ca467a67", null ],
    [ "createBatchedNMSPlugin", "_nv_infer_plugin_8h.html#aa2f0a45df014ea2e3810f9df821d0514", null ],
    [ "createClipPlugin", "_nv_infer_plugin_8h.html#af308dcae61dab659073bc91c6ba63a7e", null ],
    [ "createInstanceNormalizationPlugin", "_nv_infer_plugin_8h.html#aa240c555f42e555583395e85b2bc98b5", null ],
    [ "createLReLUPlugin", "_nv_infer_plugin_8h.html#a3f08d09a6da952b2ecb4036f92951937", null ],
    [ "createNMSPlugin", "_nv_infer_plugin_8h.html#a91af500ee7870767235becf04b764f7e", null ],
    [ "createNormalizePlugin", "_nv_infer_plugin_8h.html#a23fc3c32fb290af2b0f6f3922b1e7527", null ],
    [ "createPriorBoxPlugin", "_nv_infer_plugin_8h.html#a45f385b18503d44a09d7d24399886cc6", null ],
    [ "createRegionPlugin", "_nv_infer_plugin_8h.html#ac807ac1d28f6858dbeb023f5783b9ce4", null ],
    [ "createReorgPlugin", "_nv_infer_plugin_8h.html#aa629116455da2f2a45f7385e56af68bb", null ],
    [ "createRPNROIPlugin", "_nv_infer_plugin_8h.html#a9aa5c59fe1c3e4507981f83d694ffccc", null ],
    [ "createSplitPlugin", "_nv_infer_plugin_8h.html#a2a00a522c22a84971322c3d86188100c", null ],
    [ "initLibNvInferPlugins", "_nv_infer_plugin_8h.html#a5238fbc83e103bce4c85291c8c5b85a6", null ]
];