var _nv_infer_version_8h =
[
    [ "NV_TENSORRT_BUILD", "_nv_infer_version_8h.html#a565b16525ce7c9362f01af600ed5d752", null ],
    [ "NV_TENSORRT_MAJOR", "_nv_infer_version_8h.html#aca5940a61fa51e91f41d88d9198bf935", null ],
    [ "NV_TENSORRT_MINOR", "_nv_infer_version_8h.html#a7df0f049b87bee17d6aed394544e8979", null ],
    [ "NV_TENSORRT_PATCH", "_nv_infer_version_8h.html#af4133629ec84b76f9f13d6eb6519764f", null ],
    [ "NV_TENSORRT_SONAME_MAJOR", "_nv_infer_version_8h.html#a02c4c2cd27f4dfcd7796fe59a4802d13", null ],
    [ "NV_TENSORRT_SONAME_MINOR", "_nv_infer_version_8h.html#ae4094e625f97b65aba644f1a9c6e1048", null ],
    [ "NV_TENSORRT_SONAME_PATCH", "_nv_infer_version_8h.html#afaa148d3f876dbeda6e898b6b567e94a", null ]
];