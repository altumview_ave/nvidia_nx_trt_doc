var searchData=
[
  ['readcalibrationcache_1375',['readCalibrationCache',['../classnvinfer1_1_1_i_int8_calibrator.html#a68fae1bf82e317a0e3ca49af1d18d3cd',1,'nvinfer1::IInt8Calibrator']]],
  ['readhistogramcache_1376',['readHistogramCache',['../classnvinfer1_1_1_i_int8_legacy_calibrator.html#af5438fd3d9fb950e8c841ae458a1ce55',1,'nvinfer1::IInt8LegacyCalibrator']]],
  ['reduceverbosity_1377',['reduceVerbosity',['../classnvonnxparser_1_1_i_onnx_config.html#a9f3c0704086abac0f8a60fe3fa12c4e2',1,'nvonnxparser::IOnnxConfig']]],
  ['refitcudaengine_1378',['refitCudaEngine',['../classnvinfer1_1_1_i_refitter.html#acb526579a0573921bb1ce8e8f21676ae',1,'nvinfer1::IRefitter']]],
  ['registercreator_1379',['registerCreator',['../classnvinfer1_1_1_i_plugin_registry.html#ad506e55b00837503c310eda313c270ee',1,'nvinfer1::IPluginRegistry']]],
  ['registerinput_1380',['registerInput',['../classnvuffparser_1_1_i_uff_parser.html#aa88fc7901e6805be9e093dd0b98310e3',1,'nvuffparser::IUffParser']]],
  ['registeroutput_1381',['registerOutput',['../classnvuffparser_1_1_i_uff_parser.html#a020511b8334f287420578aa38a718c4d',1,'nvuffparser::IUffParser']]],
  ['removetensor_1382',['removeTensor',['../classnvinfer1_1_1_i_network_definition.html#a17e88f382119792187af786c3cc83770',1,'nvinfer1::INetworkDefinition']]],
  ['reordersubbuffers_1383',['reorderSubBuffers',['../_nv_utils_8h.html#a77eef19033418f615a81965eebb70fb5',1,'nvinfer1::utils']]],
  ['reportalgorithms_1384',['reportAlgorithms',['../classnvinfer1_1_1_i_algorithm_selector.html#a6abba6ab3d7385f9ca8bc3787380505a',1,'nvinfer1::IAlgorithmSelector']]],
  ['reporterror_1385',['reportError',['../classnvinfer1_1_1_i_error_recorder.html#acb33e889a2a42eaf573a9c1b8326ec72',1,'nvinfer1::IErrorRecorder']]],
  ['reportlayertime_1386',['reportLayerTime',['../classnvinfer1_1_1_i_profiler.html#ae5991e8feb7af5d029eac8972240d986',1,'nvinfer1::IProfiler']]],
  ['reset_1387',['reset',['../classnvinfer1_1_1_i_builder_config.html#ab264afe443f438c24a360612474ca85d',1,'nvinfer1::IBuilderConfig::reset()'],['../classnvinfer1_1_1_i_builder.html#afe534cf3bf66e32fd57e828a6084fd42',1,'nvinfer1::IBuilder::reset(nvinfer1::INetworkDefinition &amp;network)=0'],['../classnvinfer1_1_1_i_builder.html#ae66de42be79263b8c8818ad15538f0fd',1,'nvinfer1::IBuilder::reset()=0']]],
  ['resetdevicetype_1388',['resetDeviceType',['../classnvinfer1_1_1_i_builder_config.html#aecc7aced010281ce1b62e0ca3b712d28',1,'nvinfer1::IBuilderConfig::resetDeviceType()'],['../classnvinfer1_1_1_i_builder.html#a968fb89a2e8a9cab07c1d67d93adf903',1,'nvinfer1::IBuilder::resetDeviceType()']]],
  ['resetdynamicrange_1389',['resetDynamicRange',['../classnvinfer1_1_1_i_tensor.html#a5cae811ed6471d916c9ceeb622ef0692',1,'nvinfer1::ITensor']]],
  ['resetoutputtype_1390',['resetOutputType',['../classnvinfer1_1_1_i_layer.html#a6b21431f89512e2503923fa363ead0c6',1,'nvinfer1::ILayer']]],
  ['resetprecision_1391',['resetPrecision',['../classnvinfer1_1_1_i_layer.html#ab2574cca5a8d31768a9037957fedf8af',1,'nvinfer1::ILayer']]],
  ['reshapeweights_1392',['reshapeWeights',['../_nv_utils_8h.html#a6e6adbba162bae91ca9c9fb605406dc7',1,'nvinfer1::utils']]]
];
